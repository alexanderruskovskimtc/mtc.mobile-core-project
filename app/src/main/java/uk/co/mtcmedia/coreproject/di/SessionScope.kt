package uk.co.mtcmedia.coreproject.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class SessionScope {}