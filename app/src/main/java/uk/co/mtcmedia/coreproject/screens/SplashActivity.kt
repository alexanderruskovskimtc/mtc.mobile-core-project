package uk.co.mtcmedia.coreproject.screens
/**
 * Created by Aleksandar Ruskovski on 18,September,2018
 */

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import uk.co.mtcmedia.coreproject.R
import uk.co.mtcmedia.coreproject.screens.login.LoginActivity

class SplashActivity : ActivityBase() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed({
            goToLoginActivity()
        },3000)
    }

    fun goToLoginActivity(){
        val loginIntent = Intent(this, LoginActivity::class.java)
        loginIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(loginIntent)
        finish()
    }

}
