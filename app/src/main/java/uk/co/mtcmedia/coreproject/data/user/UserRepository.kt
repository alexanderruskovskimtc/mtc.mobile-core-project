package uk.co.mtcmedia.coreproject.data.user

import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import uk.co.mtcmedia.coreproject.App
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(context:App){

    private val sharedPreferences = context.getSharedPreferences("user",0)

    var user:User = constructUser()
        set(value) {
            field = value
            when(value){
                is NoUser -> clearUserData()
                is UserInstance -> saveUser(value)
            }
            subject.onNext(value)
        }

    private val subject = BehaviorSubject.createDefault(user).toSerialized()




    fun asObservable(): Observable<User> = subject


    private fun constructUser(): User {
        return try{
            UserInstance(
                    sessionToken = sharedPreferences.getString(UserInstance.sessionToken,null),
                    username =  sharedPreferences.getString(UserInstance.username,null),
                    password = sharedPreferences.getString(UserInstance.password,null)
            )
        }catch (ex:Exception){
            NoUser
        }
    }

    private fun saveUser(user:UserInstance){
        sharedPreferences.edit()
                .putString(UserInstance.sessionToken,user.sessionToken)
                .putString(UserInstance.username,user.username)
                .putString(UserInstance.password,user.password)
                .apply()
    }

    private fun clearUserData(){
        sharedPreferences.edit().clear().apply()
    }
}