package uk.co.mtcmedia.coreproject.screens.fragments.landing

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import butterknife.ButterKnife
import uk.co.mtcmedia.coreproject.R
import uk.co.mtcmedia.coreproject.screens.FragmentBase
import uk.co.mtcmedia.coreproject.screens.main.MainActivity
import javax.inject.Inject

/**
 * Created by Aleksandar Ruskovski on 19,September,2018
 */

class MainFragment : FragmentBase(){

    override val fragmentTitle: String get() = "Home"
    override val backstackTag: String get() = "mainFragment"

    @Inject lateinit var presenter:MainFragmentPresenter

    companion object {
        fun getInstance():MainFragment{
            val bundle = Bundle()
            val frag = MainFragment()
            frag.arguments = bundle
            return frag
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val session = sessionInjection { it.inject(this) }
        if(!session){(activity as MainActivity).sessionKilled()}
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ButterKnife.bind(this,view)
    }

    fun render(state:MainFragmentViewState){

    }
}