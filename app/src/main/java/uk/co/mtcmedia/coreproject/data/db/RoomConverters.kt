package uk.co.mtcmedia.coreproject.data.db

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import uk.co.mtcmedia.coreproject.data.user.NoUser
import uk.co.mtcmedia.coreproject.data.user.User
import uk.co.mtcmedia.coreproject.data.user.UserInstance

object RoomConverters {

    @TypeConverter
    fun userToJson(user: UserInstance): String {
        return Gson().toJson(user)
    }

    @TypeConverter
    fun jsonToUserInstance(stringUser: String): User {
        if(stringUser.length <10)
            return NoUser
        val type = object : TypeToken<UserInstance>() {}.type
        return Gson().fromJson(stringUser, type)
    }
}