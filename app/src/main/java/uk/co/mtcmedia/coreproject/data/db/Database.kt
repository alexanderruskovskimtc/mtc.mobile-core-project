package uk.co.mtcmedia.coreproject.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverter
import android.arch.persistence.room.TypeConverters
import uk.co.mtcmedia.coreproject.data.user.User
import uk.co.mtcmedia.coreproject.data.user.UserInstance
//
//@Database(entities = [UserInstance::class],version = 1)
//@TypeConverters(RoomConverters::class)
//abstract class Database :RoomDatabase(){
//    abstract fun userDao() : UserDao
//}