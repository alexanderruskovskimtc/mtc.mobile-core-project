package uk.co.mtcmedia.coreproject.di

import dagger.Module
import dagger.Provides
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import uk.co.mtcmedia.coreproject.data.user.UserInstance

@Module
class SessionModule(user:UserInstance){

    private val userSubject :Subject<UserInstance> = BehaviorSubject
            .createDefault<UserInstance>(user)
            .toSerialized()

    private val sessionToken:String = user.sessionToken
    private val scopeDisposable = CompositeDisposable()

    fun terminate(){
        scopeDisposable.dispose()
        userSubject.onComplete()
    }

    fun next(user:UserInstance){
        userSubject.onNext(user)
    }

    @Provides
    @SessionScope
    fun scopeDisposable():CompositeDisposable = scopeDisposable

    @Provides
    @SessionScope
    fun sessionToken():String = sessionToken

    @Provides
    @SessionScope
    fun providesUserStream(): Observable<UserInstance> = userSubject

}