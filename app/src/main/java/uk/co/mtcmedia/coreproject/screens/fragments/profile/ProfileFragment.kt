package uk.co.mtcmedia.coreproject.screens.fragments.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife
import uk.co.mtcmedia.coreproject.R
import uk.co.mtcmedia.coreproject.screens.FragmentBase

/**
 * Created by Aleksandar Ruskovski on 20,September,2018
 */
class ProfileFragment : FragmentBase(){
    override val backstackTag: String get() = "profileFragment"
    override val fragmentTitle: String get() = "Profile"

    companion object {
        fun getInstance():ProfileFragment{
            val bundle = Bundle()
            val frag = ProfileFragment()
            frag.arguments = bundle
            return frag
        }
    }

    override fun onStart() {
        super.onStart()
        val session = sessionInjection { it.inject(this) }
        if(!session) {
            sessionFinished()
            return
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.profile_fragment,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ButterKnife.bind(this,view)
    }
}