package uk.co.mtcmedia.coreproject.logic

import retrofit2.http.Body
import retrofit2.http.POST
import rx.Single
import uk.co.mtcmedia.coreproject.Constants
import uk.co.mtcmedia.coreproject.logic.usecases.UCLogin

interface Api {

    @POST(Constants.LOG_IN)
    fun login(@Body request: UCLogin.Request): Single<UCLogin.Response>


}