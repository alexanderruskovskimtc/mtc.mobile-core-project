package uk.co.mtcmedia.coreproject.screens.fragments.landing

import uk.co.mtcmedia.coreproject.data.user.UserRepository
import uk.co.mtcmedia.coreproject.di.SessionScope
import javax.inject.Inject

/**
 * Created by Aleksandar Ruskovski on 19,September,2018
 */
@SessionScope
class MainFragmentPresenter @Inject constructor(val userRepository: UserRepository){

    fun destroySession(){
    }


}