package uk.co.mtcmedia.coreproject.screens.login

/**
 * Created by Aleksandar Ruskovski on 18,September,2018
 */

data class LoginViewState(
        val loading:Boolean = false,
        val loginSuccessful:Boolean = false,
        val message:String ?= null
)