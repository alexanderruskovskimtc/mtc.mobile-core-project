package uk.co.mtcmedia.coreproject.screens

import android.support.v4.app.Fragment
import android.widget.Toast
import uk.co.mtcmedia.coreproject.App
import uk.co.mtcmedia.coreproject.di.SessionComponent
import uk.co.mtcmedia.coreproject.screens.main.MainActivity

/**
 * Created by Aleksandar Ruskovski on 19,September,2018
 */

abstract class FragmentBase: Fragment(){

    protected fun sessionInjection(injection: (SessionComponent) -> Unit):Boolean{
        val component = App.getSessionComponent()
        if(component != null){
            injection.invoke(component)
            return true
        }else{
            sessionFinished()
            return false
        }
    }

    protected fun sessionFinished(){
        (activity as MainActivity).sessionKilled()
    }

    abstract val backstackTag:String
    abstract val fragmentTitle:String
}