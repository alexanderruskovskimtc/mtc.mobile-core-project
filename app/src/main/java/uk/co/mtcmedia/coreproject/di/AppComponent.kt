package uk.co.mtcmedia.coreproject.di

import dagger.Component
import uk.co.mtcmedia.coreproject.data.user.UserRepository
import uk.co.mtcmedia.coreproject.screens.login.LoginActivity
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetworkModule::class, DataModule::class])
interface AppComponent {

    fun inject(loginActivity: LoginActivity)
    fun userRepository():UserRepository

    fun plus(sessionModule: SessionModule): SessionComponent

}