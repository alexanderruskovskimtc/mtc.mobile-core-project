package uk.co.mtcmedia.coreproject.di

import android.content.SharedPreferences
import android.preference.PreferenceManager
import dagger.Module
import dagger.Provides
import uk.co.mtcmedia.coreproject.App
import javax.inject.Singleton

@Singleton
@Module
class AppModule constructor(val app: App){


    @Provides
    @Singleton
    fun providesApp():App = app

    @Provides
    @Singleton
    fun providesSharedPreferences(app:App):SharedPreferences = PreferenceManager.getDefaultSharedPreferences(app)

}