package uk.co.mtcmedia.coreproject.di

import android.os.Build
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import uk.co.mtcmedia.coreproject.App
import uk.co.mtcmedia.coreproject.BuildConfig
import uk.co.mtcmedia.coreproject.Constants
import uk.co.mtcmedia.coreproject.logic.Api
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Singleton
@Module
class NetworkModule{

    val CACHE_SIZE :Long = 1024 * 1024 * 48 //48mb cache

    @Provides
    @Singleton
    fun providesOkHttpClient(app:App):OkHttpClient{
        val builder = OkHttpClient.Builder()
        val cachce = Cache(app.cacheDir,CACHE_SIZE)
        if(BuildConfig.DEBUG){
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(interceptor)
        }
        return builder
                .cache(cachce)
                .connectTimeout(10,TimeUnit.SECONDS)
                .build()
    }

    @Provides
    @Singleton
    fun providesGson(): Gson{
        return GsonBuilder()
                .setLenient() //make it liberal to what it's accept. By default it's accepting JSON
                .create()

    }

    @Provides
    @Singleton
    fun providesPicasso(app:App,okHttpClient: OkHttpClient):Picasso{
        return Picasso.Builder(app)
                .downloader(OkHttp3Downloader(okHttpClient))
                .build()
    }

    @Provides
    @Singleton
    fun providesApi(gson: Gson,okHttpClient:OkHttpClient):Api{
        val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .baseUrl(Constants.API_BASE_URL) // this might be ignored in almost all cases
                .build()
        return retrofit.create(Api::class.java)
    }



}
