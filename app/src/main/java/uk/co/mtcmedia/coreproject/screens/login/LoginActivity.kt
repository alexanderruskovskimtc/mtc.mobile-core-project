package uk.co.mtcmedia.coreproject.screens.login
/**
 * Created by Aleksandar Ruskovski on 18,September,2018
 */




import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import uk.co.mtcmedia.coreproject.R

import kotlinx.android.synthetic.main.activity_login.*
import uk.co.mtcmedia.coreproject.App
import uk.co.mtcmedia.coreproject.data.user.NoUser
import uk.co.mtcmedia.coreproject.data.user.User
import uk.co.mtcmedia.coreproject.data.user.UserInstance
import uk.co.mtcmedia.coreproject.data.user.UserRepository
import uk.co.mtcmedia.coreproject.screens.ActivityBase
import uk.co.mtcmedia.coreproject.screens.main.MainActivity
import uk.co.mtcmedia.coreproject.screens.registration.RegistrationActivity
import java.nio.channels.ShutdownChannelGroupException
import javax.inject.Inject

class LoginActivity : ActivityBase(){

    @Inject lateinit var userRepository :  UserRepository


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        App.getAppComponent().inject(this)
        tvLogin.setOnClickListener(this::onLoginClicked)
        tvForgotPassword.setOnClickListener(this::onRestartPassword)
        tvCreateAccount.setOnClickListener(this::onRegisterClicked)
        root.setOnClickListener(this::onRootClicked)
        userRepository.asObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{
                    when(it){
                        is UserInstance -> goToMainActivity()
                        is NoUser ->{Toast.makeText(this,"There is no user",Toast.LENGTH_LONG).show()}
                    }
                }
    }


    fun onLoginClicked(view:View){
        progressBar2.visibility = View.VISIBLE
        val user =  UserInstance(sessionToken = "sessionToken+23424234",username = etUsername.editText?.text.toString(),password = etPassword.editText?.text.toString())
        Handler().postDelayed({
            userRepository.user = user
        },3000)
    }

    fun onRestartPassword(view:View){

    }

    fun onRegisterClicked(view:View) {
        val intent = Intent(this, RegistrationActivity::class.java)
        startActivity(intent)
    }

    fun goToMainActivity(){
        val intent = Intent(this,MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun onRootClicked(view:View){
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}

