package uk.co.mtcmedia.coreproject.screens.main

import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.widget.Toast
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.drawer_header.*
import uk.co.mtcmedia.coreproject.App
import uk.co.mtcmedia.coreproject.R
import uk.co.mtcmedia.coreproject.data.user.UserInstance
import uk.co.mtcmedia.coreproject.data.user.UserRepository
import uk.co.mtcmedia.coreproject.screens.ActivityBase
import uk.co.mtcmedia.coreproject.screens.FragmentBase
import uk.co.mtcmedia.coreproject.screens.fragments.landing.MainFragment
import uk.co.mtcmedia.coreproject.screens.fragments.profile.ProfileFragment
import uk.co.mtcmedia.coreproject.screens.fragments.settings.SettingsFragment
import javax.inject.Inject

/**
 * Created by Aleksandar Ruskovski on 18,September,2018
 */

public class MainActivity: ActivityBase(){

    @Inject lateinit var presenter:MainActivityPresenter
    @Inject lateinit var userRepository: UserRepository

    var lastFragment:FragmentBase ?= null


    val homeFragment = MainFragment.getInstance()
    val profileFragment = ProfileFragment.getInstance()
    val settingsFragment = SettingsFragment.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        val validSession = sessionInjection { it.inject(this) }
        if(!validSession) return

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        setToolbarTitle("Home")
        val toggle = ActionBarDrawerToggle(this,drawer_layout,toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        navigationView.setNavigationItemSelectedListener {
            it.isChecked = true
            closeDrawer()
            when(it.itemId){
                R.id.drawerUserProfile -> {onProfileClicked(); true}
                R.id.drawerSettings -> {onSettingsClicked();true}
                R.id.drawerLogout -> {onLogoutClicked();true}
                else -> {
                    false
                }
            }
        }

        val headerView = navigationView.getHeaderView(0)
        val drawerUsername = headerView.findViewById<TextView>(R.id.tvDrawerUsername)
        drawerUsername.text = ((userRepository.user) as UserInstance).username

        showFragmet(homeFragment,false)
        navigationView.menu.getItem(0).isChecked = true
    }

    fun setToolbarTitle(title:String){
        supportActionBar?.title = title
    }

    fun showFragmet(fragment:FragmentBase,addToBackstack:Boolean=true){
        lastFragment = fragment
        val transaction = supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in_short, R.anim.fade_out_short,R.anim.fade_in_short, R.anim.fade_out_short)
                .replace(R.id.fragmentHolder, fragment)
        if(addToBackstack){
            //transaction.addToBackStack(fragment.backstackTag)
        }
        transaction.commit()
        setToolbarTitle(fragment.fragmentTitle)
    }

    fun closeDrawer(){
        drawer_layout.closeDrawer(GravityCompat.START)
    }


    /**  DRAWER CLICK*/
    fun onProfileClicked(){
        showFragmet(profileFragment)
    }
    fun onSettingsClicked(){
        showFragmet(settingsFragment)
    }
    fun onLogoutClicked(){
        presenter.logout()
    }

    /**END OF DRAWER CLICKS*/

    override fun onBackPressed() {
        if(drawer_layout.isDrawerOpen(GravityCompat.START))
            closeDrawer()
        else
            super.onBackPressed()
    }
}
