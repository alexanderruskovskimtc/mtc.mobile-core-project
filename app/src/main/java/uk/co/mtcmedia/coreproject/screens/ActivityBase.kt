package uk.co.mtcmedia.coreproject.screens

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import uk.co.mtcmedia.coreproject.App
import uk.co.mtcmedia.coreproject.di.SessionComponent
import uk.co.mtcmedia.coreproject.screens.login.LoginActivity

/**
 * Created by Aleksandar Ruskovski on 18,September,2018
 */

open class ActivityBase : AppCompatActivity(){


    protected fun sessionInjection(injection: (SessionComponent) -> Unit):Boolean{
        val component = App.getSessionComponent()
        if(component != null){
            injection.invoke(component)
            return true
        }else{
            sessionFinished()
            return false
        }
    }

    protected fun sessionFinished(){
        Toast.makeText(this,"Sessions is destroyed from sessionFinished",Toast.LENGTH_LONG).show()
    }

    fun sessionKilled(){
        val intent = Intent(this,LoginActivity::class.java)
        startActivity(intent)
        this.finish()
    }

}