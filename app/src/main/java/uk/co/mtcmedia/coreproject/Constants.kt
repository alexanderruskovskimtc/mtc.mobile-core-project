package uk.co.mtcmedia.coreproject

object Constants {

    val API_BASE_URL = ""

    const val LOG_IN = "login.json"
    const val REGISTER = "register.json"

}