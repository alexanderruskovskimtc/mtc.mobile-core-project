package uk.co.mtcmedia.coreproject

import android.app.Application
import io.reactivex.android.schedulers.AndroidSchedulers
import uk.co.mtcmedia.coreproject.data.user.NoUser
import uk.co.mtcmedia.coreproject.data.user.UserInstance
import uk.co.mtcmedia.coreproject.di.*

class App: Application(){

    companion object {
        private lateinit var app:App
        fun getAppComponent():AppComponent = app.appComponent
        fun getSessionComponent():SessionComponent? = app.sessionComponent
    }

    private lateinit var appComponent:AppComponent

    private var sessionComponent: SessionComponent ?= null
    private var sessionModule: SessionModule? = null

    override fun onCreate() {
        super.onCreate()
        App.app = this
        initDagger()
    }

    fun initDagger(){
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .networkModule(NetworkModule())
                .build()

        appComponent.userRepository().asObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{user ->
                    when(user){
                        is NoUser ->{
                            sessionModule?.terminate()
                            sessionModule = null
                            sessionComponent = null
                        }
                        is UserInstance ->{
                            val sessionModule = this.sessionModule
                            if(sessionModule == null){
                                val module = SessionModule(user)
                                this.sessionModule = module
                                sessionComponent = appComponent.plus(module)
                            }else{
                                sessionModule.next(user)
                            }
                        }
                    }
                }

    }


}