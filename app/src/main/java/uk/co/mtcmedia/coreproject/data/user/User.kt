package uk.co.mtcmedia.coreproject.data.user


sealed class User

object NoUser : User()

data class UserInstance constructor(
    val sessionToken: String,
    val username: String,
    val password: String
) : User(){
    companion object {
        private const val prefix = "user."
        const val sessionToken = prefix+"sessionToken"
        const val username = prefix+"username"
        const val password = prefix+"password"
    }
}


