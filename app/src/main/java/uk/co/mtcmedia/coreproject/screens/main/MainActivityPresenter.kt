package uk.co.mtcmedia.coreproject.screens.main

import uk.co.mtcmedia.coreproject.data.user.NoUser
import uk.co.mtcmedia.coreproject.data.user.UserRepository
import uk.co.mtcmedia.coreproject.di.SessionScope
import javax.inject.Inject

/**
 * Created by Aleksandar Ruskovski on 20,September,2018
 */
@SessionScope
class MainActivityPresenter @Inject constructor(
        val userRepository: UserRepository
){

    fun logout(){
        userRepository.user = NoUser
    }



}