package uk.co.mtcmedia.coreproject.logic.usecases

public class UCLogin{

    data class Request(
            val username:String,
            val password:String
    )

    data class Response(
            val sessionToken:String,
            val successfully:Boolean
    )

}