package uk.co.mtcmedia.coreproject.screens.fragments.settings

import android.location.SettingInjectorService
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife
import uk.co.mtcmedia.coreproject.R
import uk.co.mtcmedia.coreproject.screens.FragmentBase

/**
 * Created by Aleksandar Ruskovski on 20,September,2018
 */

class SettingsFragment : FragmentBase(){
    override val backstackTag: String get() = "settingsFragment"
    override val fragmentTitle: String get() = "Settings"

    companion object {
        fun getInstance():SettingsFragment{
            val bundle = Bundle()
            val frag = SettingsFragment()
            frag.arguments = bundle
            return frag
        }
    }

    override fun onStart() {
        super.onStart()
        val session = sessionInjection { it.inject(this) }
        if(!session){
            sessionFinished()
            return
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.settings_fragment,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ButterKnife.bind(this,view)
    }
}
