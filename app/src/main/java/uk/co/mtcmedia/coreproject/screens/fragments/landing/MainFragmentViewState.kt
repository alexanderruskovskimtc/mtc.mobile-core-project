package uk.co.mtcmedia.coreproject.screens.fragments.landing

/**
 * Created by Aleksandar Ruskovski on 19,September,2018
 */

data class MainFragmentViewState(
        val loading: Boolean = false
)