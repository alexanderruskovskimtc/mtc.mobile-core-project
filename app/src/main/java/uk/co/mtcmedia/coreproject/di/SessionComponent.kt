package uk.co.mtcmedia.coreproject.di

import dagger.Component
import dagger.Subcomponent
import uk.co.mtcmedia.coreproject.screens.fragments.landing.MainFragment
import uk.co.mtcmedia.coreproject.screens.fragments.profile.ProfileFragment
import uk.co.mtcmedia.coreproject.screens.fragments.settings.SettingsFragment
import uk.co.mtcmedia.coreproject.screens.main.MainActivity
import javax.inject.Singleton

@SessionScope
@Subcomponent(modules =  [SessionModule::class])
interface SessionComponent {

    fun inject (mainActivity: MainActivity)
    fun inject (mainFragment: MainFragment)
    fun inject (profileFragment: ProfileFragment)
    fun inject (settingsFragment: SettingsFragment)

}